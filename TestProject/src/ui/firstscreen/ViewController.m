//
//  ViewController.m
//  TestProject
//
//  Created by user on 5/23/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, retain) NSMutableArray *matrix;

@end

@implementation ViewController

int size;
int x,y;

- (void)viewDidLoad {
    [super viewDidLoad];
    size = 32;
    self.matrix = [[NSMutableArray alloc] initWithCapacity:size];
    [self initMatrix];
    [self createTrueDirection:self.matrix];
    [self createWrongDirection:self.matrix];
    [self printMatrix:self.matrix];
}


# pragma mark - create matrix

-(void) initMatrix{
    for (int y = 0; y < size; y++){
        NSMutableArray *columnArray = [NSMutableArray array];
        for (int x = 0; x < size; x++){
            [columnArray addObject:@"*"];
        }
        [self.matrix addObject:columnArray];
    }
}


# pragma mark - create true direction

-(NSMutableArray *) createTrueDirection: (NSMutableArray *) matr {

    int lastPosition = 0;
    for (int y = 0; y < size - 1; y +=2 ){
        int random = arc4random_uniform(size - 2) + 1;
        if ( lastPosition != 0){
            if (lastPosition < random){
                for (int a = 0; a < random - lastPosition;a++){
                    matr[lastPosition + a][y] = @" ";
                }
            }
            else {
                for (int a = 0; a < lastPosition - random;a++){
                    matr[lastPosition - a][y] = @" ";
                }
            }
        }
        matr[random][y] = @" ";
        matr[random][y+1] = @" ";
        lastPosition = random;
    }
    
    if (size % 2 != 0) {
        matr[lastPosition][size-1] = @" ";
    }
    return matr;
}


# pragma mark - create wrong direction

-(NSMutableArray *) createWrongDirection: (NSMutableArray *) matr {
    NSMutableArray *matrWithTrueDirection = matr;
    for (int a = 0; a < size / 4 ; a++){
        int random_x = arc4random_uniform(size - 2) + 1;
        int random_y = arc4random_uniform(size - 2) + 1;
        
        x = random_x;
        y = random_y;
        
        if  ([matr [x][y]  isEqual: @"*" ]  &&  ![self isBounds]) {
            
            do{
                int random_direction = arc4random_uniform(3);
                //  [self generateRandomDirection];
                matr[x][y] = @" ";
                switch (random_direction) {
                    case 0:
                        //go down
                        y += 1;
                        break;
                    case 1:
                        //go left
                        x -=  1;
                        break;
                    case 2:
                        //go up
                        y -= 1;
                        break;
                    case 3:
                        //go right
                        x += 1;
                        break;
                    default:
                        break;
                }
            }
            // while ([matrix [x][y]  isEqual: @"*"] && ![self isBounds] );
            while ( ![self isBounds]);
        }
    }
    return matr;
}


# pragma mark - print matrix

-(void) printMatrix: (NSArray*  ) matr {
    
    NSMutableString *line = [NSMutableString stringWithString:@""];
    for (int y = 0; y < size; y++){
        for (int x = 0; x < size; x++){
            
            [line appendString: matr [x][y]];
        }
        NSLog(@"%@",line);
        [line setString:@""];
    }
}


# pragma mark - generate random direction
//TODO: change void to int

- (void) generateRandomDirection: (int) random_direction {
    random_direction = arc4random_uniform(3);
    if ([self isBounds]) {
        [self generateRandomDirection: random_direction];
    }
}


# pragma mark - check bounds

-(BOOL) isBounds {
    if (x == 0 || x == size - 1  || y == 0 || y == size - 1 ){
        return true;
    }
    return false;
}

@end
